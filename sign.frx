*sign.txt*     Pour Vim version 8.2. Dernière modification : 31 août 2020


		MANUEL de RÉFÉRENCE VIM  par Gordon Prieur
					 et  Bram Moolenaar


Fonctionnalité de support de signalisation 			*sign-support*

1. Introduction				|sign-intro|
2. Commandes				|sign-commands|
3. Fonctions				|sign-functions-details|

{uniquement si compilé avec la fonctionnalité |+signs|}

==============================================================================
1. Introduction					*sign-intro*

Lorsqu'un débogueur ou un autre outil de programmation intégré pilote un
éditeur, celui-ci doit être capable de spécifier des repères qui donneront
à l'utilisateur les informations utiles sur le fichier. Un bon exemple 
peut être un débogueur qui placerait une icône dans une colonne à gauche  
afin de signaler un point d'arrêt. Un autre exemple pourrait être le cas d'une 
flèche représentant un compteur ordinal (PC). La fonctionnalité de 
signalisation permet à la fois la définition d'un symbole ou d'une icône
sur la gauche de la fenêtre et la définition d'une mise en surbrillance 
qui sera appliquée à la ligne. L'affichage du symbole comme une image est,
le plus souvent, uniquement possible qu'avec gvim (bien que le dtterm de
Sun Microsystem supporte cela, c'est à ma connaissance le seul émulateur de
terminal qui en soit capable). Un symbole en texte et la surbrillance devrait
être possible avec n'importe quel émulateur de terminal en couleur.

Les symboles et les mises en surbrillance ne sont pas simplement utiles
pour les débogueurs. Le Visual WorkShop de Sun utilise les symboles et la
mise en surbrillance pour notifier les erreurs de compilation et les
repères de SourceBrowser. De plus, le débogueur supporte 8 à 10 symboles
et couleurs de surbrillance différents, voir |NetBeans|.

Il y a deux étapes dans l'utilisation de la signalisation :

1. Définition de la signalisation. Ceci indique l'image, le texte et la mise
   en surbrillance.  Par exemple, vous pouvez définir une signalisation "Stop"
   avec l'image d'un panneau routier STOP et le texte "!!".

2. Placer la signalisation. Ceci indique le fichier et le numero de ligne où
   la signalisation doit être affichée. Une signalisation définie peut être
   placée plusieurs fois sur différentes lignes de différents fichiers.

							*sign-column*
Lorsqu'une signalisation est définie pour un fichier, Vim va automatiquement
ajouter une colonne de deux caractères pour l'afficher. Lorsque la dernière
signalisation est retirée, la colonne disparaît. Ce comportement peut être
modifié par l'option 'signcolumn'.

La couleur de la colonne est définie par le groupe de surbrillance
SignColumn |hl-SignColumn|. Exemple pour défiir la couleur : >

	:highlight SignColumn guibg=darkgrey
<
							*sign-identifier*
Chaque signalisation placée est identifiée par un nombre appelé identifiant de
signalisation. Cet identifiant est utilisé pour se déplacer vers la
signalisaton ou pour la retirer. L'identifiant est assigné au moment du
placement de la signalisation par la commande |:sign-place| ou la fonction
|:sign-place()|. Chaque identifiant de signalisation doit être unique. Si
plusieurs signalisations placées utilise le même identifiant, le déplacement
ou le retrait deviennent imprédictible. Pour éviter le chevauchement
d'identifiants, il est possible d'utiliser les groupes de signalisation. La
fonction |sign_place()| peut être appelée avec un identifiant de signalisation
égal à zéro pour allouer le prochain identifiant disponible.

							*sign-group*
Chaque signalisation placée peut être assignée ou bien au groupe global, ou
bien à un groupe nommé. Quand on place une signalisation, si aucun nom de
groupe n'est fourni, ou si une chaine vide est utilisée, la signalisation est
placée dans le groupe global. Sinon, la signalisation est placée dans un
groupe nommé. L'identifiant de signalisation est unique au sein d'un groupe.
Le groupe de signalisation permet aux plugins Vim d'utiliser des
signalisations unique sans interférer avec les autres plugins utilisant les
signalisations.

Pour placer une signalisation dans une fenêtre popup, le nom de groupe doit
commencer par "PopUp". Les autres signalisations n'apparaissent pas dans une
fenêtre popup. Le nom de groupe "PopUpMenu" est utilisé par les fenêtre popup
ayant 'cursorline'.

							*sign-priority*
À chaque signalisation placée est assignée une priorité. Quand plusieurs
signalisations sont placées sur la même ligne, les attributs de la
signalisation de plus haute priorité sont utilisés indépendamment du groupe
de signalisation. La priorité par défaut est 10. La priorité est attribuée au
moment du placement de la signalisation.

Quand une ligne contenant une signalisation est supprimée, la signalisation
est déplacée à la ligne suivante (ou la dernière ligne du buffer, s'il n'y a
pas de ligne suivante). Si la suppression est annulée, la signalisation n'est
pas replacée.

Quand une signalisation avec surbrillance de ligne et une autre avec
surbrillance de ligne de curseur 'cursorline' sont toutes deux présentes, si
la priorité est 100 ou plus, alors la surbrillance de ligne prévaut, sinon
c'est celle de ligne de curseur. 

==============================================================================
2. Commandes					*sign-commands* *:sig* *:sign*

Voici un exemple de placement d'une signalisation "piet", affichée avec le
texte ">>", à la ligne 23 du fichier courant : >
	:sign define piet text=>> texthl=Search
	:exe ":sign place 2 line=23 name=piet file=" . expand("%:p")

Et la commande pour la supprimer : >
	:sign unplace 2

Note: La commande ":sign" ne peut pas être suivie d'une autre commande ou d'un
commentaire. Si vous en avez besoin, utilisez la commande |:execute|.


DÉFINITION D'UNE SIGNALISATION.			*:sign-define* *E255* *E160*

Voir |sign_define()| pour la fonction équivalente en Vim script.

:sign define {nom} {argument}...
		Définit une nouvelle signalisation ou place les attributs
		pour une signalisation existante. Le {nom} peut être un
		nombre (que des chiffres) ou un nom débutant par un caractère
		n'étant pas un chiffre. Les zéros à gauches sont ignorés, et
		donc "0012", "012" et "12" sont considérés comme le même nom.
		A peu près 120 signalisations différentes peuvent être 
		définies.

		Arguments acceptés:

	icon={bitmap}
		Définit le nom de fichier où l'image pixmap peut être 
		trouvée. Le bitmap doit pouvoir tenir dans l'espace de deux
		caractères. Ceci n'est pas vérifié. Si le bitmap est trop
		gros, un problème de rafraîchissement apparaîtra. Seul GTK 2
		peut mettre à l'échelle le bitmap pour tenir dans l'espace
		disponible.
			toolkit		supports ~
			GTK 1		pixmap (.xpm)
			GTK 2		many
			Motif		pixmap (.xpm)
			Win32		.bmp, .ico, .cur
					pixmap (.xpm) |+xpm_w32|

	linehl={groupe}
		Le groupe de mise en surbrillance utilisé pour la ligne
		entière où la signalisation est placée. Le plus utile est de
		définir une couleur d'arrière plan.

	text={texte}						*E239*
		Définit le texte qui sera affiché s'il n'y à pas icône ou que
		l'IHM graphique n'est pas utilisée. Ceci doit véritablement
		être deux caractères ASCII.

	texthl={groupe}
		Le groupe de mise en surbrillance utilisé pour l'élément
		texte.


SUPPRESSION D'UNE SIGNALISATION				*:sign-undefine* *E155*

Voir |sign_undefine()| pour la fonction Vim script équivalente.

:sign undefine {nom}
		Supprime une signalisation précédement définie. Si la 
		signalisation portant ce {nom} est encore placée, cela posera
		des problèmes


LISTAGE DES SIGNALISATIONS				*:sign-list* *E156*

Voir |sign_getdefine()| pour la fonction Vim script équivalente.

:sign list	Liste toutes les signalisations définies et leurs attributs.

:sign list {nom}
		Liste une signalisation définie et ses attributs.


PLACEMENT DES SIGNALISATIONS				*:sign-place* *E158*

Voir |sign_place()| pour la fonction Vim script équivalente.

:sign place {id} line={lnum} name={nom} file={fnom}
		Place la signalisation définie comme {nom} à la ligne {lnum}
		sur le fichier {fnom}. Le fichier {fnom} doit être déjà chargé
		dans un buffer. Le nom de fichier exacte doit être utilisé,
		les caractères joker, $ENV et le ~ ne sont pas développés.

		La signalisation est mémorisée sous l'{id}, celle-ci peut être
		utilisée pour des manipulations à venir. {id} doit être un
		nombre.
		C'est à l'utilisateur de veiller qu'une seul {id} est utilisée
		à la fois pour un fichier donné (si celle-ci est utilisée
		plusieurs fois, la suppression du placement devra être faite
		autant de fois et les changements peuvent ne pas fonctionner
		comme espéré).

		Les attributs optionnels suivant peuvent être placés avant
		"file=" :
			group={group}	Place la signalisation dans le groupe
					{group}
			priority={prio}	Assigne à la signalisation la priorité
					{prio}

		Par défaut, la signalisation est placée dans le groupe de
		signalisation global.

		Par défaut, la signalisation se voit attribuer une priorité de
		10. pour attribuer une priorité différente, utilisez
		"priority={prio}". La priorité est utilisée pour déterminer la
		signalisation qui est affichée quand plusieurs sont placées
		sur une même ligne.

		Exemples : >
			:sign place 5 line=3 name=sign1 file=a.py
			:sign place 6 group=g2 line=2 name=sign2 file=x.py
			:sign place 9 group=g2 priority=50 line=5
							\ name=sign1 file=a.py
 <
:sign place {id} line={lnum} name={nom} buffer={nr}
		Idem, mais utilise le tampon {nr}. Si l'argument buffer est
		absent, place la signalisation dans le tampon courant.

							*E885*
:sign place {id} name={nom} file={fnom}
		Change la signalisation {id} placée dans le fichier {fnom}
		afin d'utiliser la signalisation {nom}.
		Ceci peut être utilisé pour changer le symbole affiché sans le
		déplacer (lorsque le débogueur a stoppé sur un point d'arrêt 
		par exemple).

		L'attribut optionnel "group={group}" peut être placé devant
		"file=" pour sélectionner une signalisation dans un groupe
		particulier. L'attribut optionnel "priority={prio} peut être
		utilisé pour changer la priorité d'une signalisation
		existante.

:sign place {id} name={nom} buffer={nr}
		Idem, mais utilise le tampon {nr}. Si l'argument buffer est
		absent, place la signalisation dans le tampon courant.


SUPPRESSION DES SIGNALISATIONS				*:sign-unplace* *E159*

Voir |sign_unplace()| pour la fonction Vim script équivalente.

:sign unplace {id} file={fnom}
		Retire la signalisation {id} précédement placée dans le
                fichier {fnom}.

:sign unplace {id} group={group} file={fname}
		Idem mais retire la signalisation {id} du groupe {group}.

:sign unplace {id} group=* file={fname}
		Idem mais retire la signalisation {id} de tous les groupes.

:sign unplace * file={fname}
		Retire toutes les signalisations placées dans le fichier
		{fname}.

:sign unplace * group={group} file={fname}
		Retire toutes les signalisations placées dans le groupe
		{group} du fichier {fname}.

:sign unplace * group=* file={fname}
		Retire toutes les signalisations placées dans tous les groupes
		du fichier {fname}.

:sign unplace {id} buffer={nr}
		Retire la signalisation {id} précédement placée dans le
		tampon {nr}.

:sign unplace {id} group={group} buffer={nr}
		Retire la signalisation {id} précédemment placée dans le
		groupe {group} du tampon {nr}.

:sign unplace {id} group=* buffer={nr}
		Retire la signalisation {id} précédemment placée dans tous les
		groupes du tampon {nr}.

:sign unplace * buffer={nr}
		Retire toutes les signalisation placées du tampon {nr}.

:sign unplace * group={group} buffer={nr}
		Retire toutes les signalisations placées dans le groupe
		{group} du tampon {nr}.

:sign unplace * group=* buffer={nr}
		Retire toutes les signalisations placées dans tous les groupes
		du tampon {nr}.

:sign unplace {id}
		Retire la signalisation {id} précédement placée dans tous les
		fichiers où elle apparaît.

:sign unplace {id} group={group}
		Retire la signalisation {id} précédemment placée dans le
		groupe {group} de tous les fichiers où elle apparaissait.

:sign unplace {id} group=*
		Retire la signalisation {id} précédemment placée dans tous les
		groupes de tous les fichiers où elle apparaissait.

:sign unplace *
		Retire toutes les signalisations placées dans le groupe global
		de tous les fichiers.

:sign unplace * group={group}
		Retire toutes les signalisations placées dans le groupe
		{group] de tous les fichiers.

:sign unplace * group=*
		Retire toutes les signalisations placées dans tous les groupes
		de tous les fichiers.

:sign unplace
		Retire une signalisation placée là où se trouve le curseur. Si
		plusieurs signalisations sont placées sur la ligne, une seule
		est retirée.

:sign unplace group={group}
		Retire une signalisation placée dans le groupe {group} là où
		se trouve le curseur.

:sign unplace group=*
		Retire une signalisation placée dans n'importe quel groupe là
		où se trouve le curseur.


LISTAGE DES SIGNALISATIONS PLACÉES			*:sign-place-list*

Voir |sign_getplaced()| pour la fonction Vim script équivalente.

:sign place file={fnom}
		Liste les signalisations placées dans le fichier {fnom}.
		Voir les remarques ci-dessus sur {fnom} |:sign-frame|.

:sign place group={group} file={fname}
		Liste les signalisations dans le groupe {group} placés
		dans le fichier {fname}.

:sign place group=* file={fname}
		Liste les signalisations dans tous les groupes placées
		dans le fichier {fname}.

:sign place buffer={nr}
		Liste les signalisations placées dans le tampon {nr}.

< :sign place	List placed signs in all files.
---
:sign place group={group} buffer={nr}
		Liste les signalisations dans le groupe {group} placées
		dans le tampon {nr}.

:sign place group=* buffer={nr}
		Liste les signalisations dans tous les groupes placées
		dans le tampon {nr}.

:sign place	Liste les signalisations placées dans le groupe global
		dans tous tous les fichiers.

:sign place group={group}
		Liste les signalisations placées avec le groupe de
		signalisations {group} dans tous les fichiers.

:sign place group=*
		Liste les signalisations placées dans tous les groupes
		de signalisations dans tous les fichiers.


SAUTER VERS UNE SIGNALISATION				*:sign-jump* *E157*

:sign jump {id} file={fnom}
		Ouvre le fichier {fnom} ou saute à la fenêtre qui contient
		{fnom} et positionne le curseur sur la signalisation {id}.
		Si le fichier n'est pas affiché dans la fenêtre et que le
		fichier courant ne peut être |abandon|né, ceci échoue.

:sign jump {id} buffer={nr}
		Idem, mais utilise le tampon {nr}.

==============================================================================
3. Fonctions					*sign-functions-details*

sign_define({name} [, {dict}])				*sign_define()*
sign_define({list})
		Definit une nouvelle signalisation {name} ou modifie les
		attributs d'une signalisaton existante. Ceci est similaire
		à la commande |:sign-define|.

		Préfixez {name} avec un texte unique pour éviter les
		collisions de nom. Il n'y a pas de {group} comme avec le
		placement de signalisations.

		Le nom {name} peut être de type String ou Number. l'argument
		optionnel {dict} spécifie les attributs de la signalisation.
		Les valeurs suivantes sont supportées :
		   icon		chemin complet vers le fichier bitmap pour la
				signalisation.
		   linehl	groupe de surbrillance utilisé pour toute la
				ligne dans laquelle la signalisation est
				placée.
		   text		texte affiché en l'absence d'icône ou
				d'interface graphique.
		   texthl	groupe de surbrillance utilisé pour le texte.

		S'il existe déjà une signalisation nommée {name}, les
		attributs de la signalisation sont mis à jour.

		L'argument {list} peut être utilisé pour définir une liste de
		signalisations. Chaque élémént de la liste est un dictionaire
		contenant les éléments ci-dessus dans {dict} et un élément
		"name" pour le nom de la signalisation.

		Renvoie 0 en cas de succès et -1 en cas d'échec. Si l'argument
		{list} est utilisé, renvoie une liste de valeurs, une par
		signalisation définie.

		Exemples: >
			call sign_define("mySign", {
				\ "text" : "=>",
				\ "texthl" : "Error",
				\ "linehl" : "Search"})
			call sign_define([
				\ {'name' : 'sign1',
				\  'text' : '=>'},
				\ {'name' : 'sign2',
				\  'text' : '!!'}
				\ ])
<
		Peut également être utilisé comme méthode |method| : >
			GetSignList()->sign_define()

sign_getdefined([{name}])				*sign_getdefined()*
		Renvoie une list des signalisations définies et de leurs
		attributs. Similaire à la commande |:sign-list|.

		Si {name} est absent, revoie une liste de toutes les
		signalisations définies. Sinon l'attribut de la signalisation
		donnée est renvoyé.

		Chaque élément de la liste retournée est un dictionnaire
		contenant les entrées suivantes :
		   icon		chemin complet vers le fichier bitmap pour la
				signalisation.
		   linehl	groupe de surbrillance utilisé pour toute la
				ligne dans laquelle la signalisation est
				placée.
		   text		texte affiché en l'absence d'icône ou
				d'interface graphique.
		   texthl	groupe de surbrillance utilisé pour le texte.

		Renvoie une liste vide s'il n'y a pas de signalisation ou si
		{name} n'est pas trouvé.

		Exemples: >
			" Renvoie une liste de toutes les signalisations
			" définies
			echo sign_getdefined()

			" Renvoie l'attribut de la signalisation nommée
			" mySign
			echo sign_getdefined("mySign")
<
		Peut également s'utiliser comme méthode |method| : >
			GetSignList()->sign_getdefined()

sign_getplaced([{expr} [, {dict}]])			*sign_getplaced()*
		Renvoie une liste de signalisation placée dans un tampon ou
		tous les tampons. Similaire à la commande |:sign-place-list|.

		Si le nom de tampon optionnel {expr} est donné, seules les
		listes de signalisations placées dans ce buffer sont renvoyées.
		Pour l'utilisation de {expr}, voir |bufname()|. Le dictionaire
		optionnel {dict} peut contenir les entrées suivantes :
		   group	selectionne seulement les signalisations dans
				ce groupe
		   id		selectionne la signalisation ayant cet
				identifiant
		   lnum		selectionne les signalisations placées sur
				cette ligne. pour l'utilisation de {lnum},
				voir |line()|.
		si {group} vaut '*', les signalisations de tous les groupes y
		compris le groupe global sont retournées. Si {group} n'est pas 
		donné ou vaut la chaine vide, seules les signalisations du
		groupe global sont retournées. Si aucun argument n'est donné,
		les signalisations du groupe global placées dans tous les
		tampons sont retournées. Voir |sign-group|.

		Chaque élément de la liste retournée est un dictionnaire
		contenant les entrées suivantes :
			bufnr	numéro du tampon contenant la signalisation
			signs	liste de signalisations placées dans {bufnr}.
				Chaque élément de la liste est un dictionnaire
				contenant les entrées listées ci-dessous

		Le dictionnaire de chaque signalisation contient les entrées
		suivantes :
			group	 groupe de signalisation. '' pour le groupe
				 global
			id	 identifiant de signalisation
			lnum	 numéro de la ligne de la signalisation
			name	 nom de la signalisation définie
			priority priorité de la signalisation

		Les signalisations retournées dans un tampon sont ordonnées
		par leur numéro et priorité.

		Renvoie une liste vide en cas d'échec ou d'absence de
		signalisation.

		Exemples: >
			" Liste des signalisations placées dans eval.c dans le
			" groupe global
			echo sign_getplaced("eval.c")

			" Liste des signalisations du groupe 'g1' placées dans
			" eval.c
			echo sign_getplaced("eval.c", {'group' : 'g1'})

			" Liste des signalisations placées à la ligne 10 de
			" eval.c
			echo sign_getplaced("eval.c", {'lnum' : 10})

			" Signalisation d'identifiant 10 placée dans a.py
			echo sign_getplaced("a.py", {'id' : 10})

			" Signalisation d'identifiant 20 du groupe 'g1' placée
			" dans a.py
			echo sign_getplaced("a.py", {'group' : 'g1',
							\  'id' : 20})

			" Liste de toutes les signalisations placées
			echo sign_getplaced()
<
		Peut également être utilisée comme méthode |method| : >
			GetBufname()->sign_getplaced()
<
							*sign_jump()*
sign_jump({id}, {group}, {expr})
		Ouvre le tampon {expr} ou saute vers la fenêtre qui contient
		{expr} et positionne le curseur à la signalisation {id} dans
		le groupe {group}.
		Similaire à la commande |:sign-jump|.

		Pour l'utilisation de {expr}, voir |bufname()|.

		Renvoie le numéro de ligne de la signalisation. Renvoie
		-1 si les arguments sont invalides.

		Exemple: >
			" Saute à la signalisation 10 dans le tampon courant
			call sign_jump(10, '', '')
<
		Peut également être utilisée en tant que méthode |method|: >
			GetSignid()->sign_jump()
<
							*sign_place()*
sign_place({id}, {group}, {name}, {expr} [, {dict}])
		Place la signalisation définie comme {name} à la ligne {lnum}
		dans le fichier ou tampon {expr} et assigne {id} et {group}
		à la signalisation. Similaire à la commande |:sign-place|.

		Si l'identifiant de signalisation {id} est zéro, un nouvel
		identifiant est alloué. Sinon, le numéro spécifié est utilisé.
		{group} est le nom du groupe de signalisation. Pour utiliser
		le groupe global, utiliser la chaîne vide. {group} fonctionne
		comme espace de nommage pour {id}. En conséquence, deux
		groupes peuvent utiliser les mêmes IDs. Voir |sign-identifier|
		et |sign-group| pour plus d'information.

		{name} fait référence à une signalisation définie.
		{expr} fait référence à un nom de tampon ou un numéro. Pour
		les valeurs acceptées, voir |bufname()|.

		L'argument optionnel {dict} accepte les entrées suivantes :
			lnum		numéro de ligne dans le fichier ou
					tampon {expr} où les signalisations
					sont placées.
					Pour les valeurs possibles, voir
					|line()|.
			priority	priorité de la signalisation. Voir
					|sign-priority| pour plus
					d'information.

		Si l'argument optionnel {dict} n'est pas donné, cela modifie
		la signalisation placées {id} dans le groupe {group} pour 
		utiliser la signalisation définie {name}.

		Renvoie l'identifiant de signalisation en cas de succès, -1
		en cas d'échec.

		Exemples: >
			" Place une signalisation sign1 avec l'id 5 à la
			" ligne 20 dans le tampon json.c
			call sign_place(5, '', 'sign1', 'json.c',
							\ {'lnum' : 20})

			" Updates sign 5 in buffer json.c to use sign2
			call sign_place(5, '', 'sign2', 'json.c')

			" Place a sign named sign3 at line 30 in
			" buffer json.c with a new identifier
			let id = sign_place(0, '', 'sign3', 'json.c',
							\ {'lnum' : 30})

			" Place a sign named sign4 with id 10 in group 'g3'
			" at line 40 in buffer json.c with priority 90
			call sign_place(10, 'g3', 'sign4', 'json.c',
					\ {'lnum' : 40, 'priority' : 90})
<
		Peut également être utilisée comme méthode |method| : >
			GetSignid()->sign_place(group, name, expr)
<
							*sign_placelist()*
sign_placelist({list})
		Place une ou plusieurs signalisations. Similaire à la fonction
		|sign_place()|.  L'argument {list} précise la liste de 
		signalisations à placer. Chaque élément de la liste est un
		dictionaire contenant les attributs de signalisation
		suivants :
		    buffer	nom ou numéro de tampon. Pour les valeurs
				possibles voir |bufname()|.
		    group	groupe de signalisations. {group} functionne
				comme un espace de nommage pour {id}, ainsi
				deux groupes peuvent utiliser les même IDs. 
				Le groupe global est utilisé si l'argument est
				absent ou vaut la chaine vide. Voir 
				|sign-group| pour plus d'information.
		    id		identifiant de signalisation. Si non spécifié,
				un nouvel identifiant unique est utilisé,
				sinon le numéro donné est utilisé. Voir
				|sign-identifier| pour plus d'information.
		    lnum	numéro de ligne dans le tampon {expr} où la
				signalisation sera placée. Pour les valeurs
				possibles voir |line()|.
		    name	nom de la signalisation à placer. Voir
				|sign_define()|pour plus d'information.
		    priority	priorité de la signalisation. Quand plusieurs
				signalisations sont placées sur une ligne,
				celle de plus haute priorité est utilisée. Si
				non précisé, la valeur par défaut 10 est
				utilisée. Voir |sign-priority| pour plus
				d'information.

		Si {id} désigne une signalisation existante, celle-ci est
		modifiée pour utiliser le nom {name} et/ou la priorité 
		{priority}.

		Renvoie une liste d'identifiants de signalisations. En cas
		d'échec, l'élément correspondant vaut -1.

		Exemples: >
			" Place la signalisation s1 avec l'id 5 ligne
			" 20 et l'id 10 ligne 30 dans a.c
			let [n1, n2] = sign_placelist([
				\ {'id' : 5,
				\  'name' : 's1',
				\  'buffer' : 'a.c',
				\  'lnum' : 20},
				\ {'id' : 10,
				\  'name' : 's1',
				\  'buffer' : 'a.c',
				\  'lnum' : 30}
				\ ])

			" Place la signalisation s1 dans le tampon a.c 
			" lignes 40 et 50
			" avec des ids auto-générés
			let [n1, n2] = sign_placelist([
				\ {'name' : 's1',
				\  'buffer' : 'a.c',
				\  'lnum' : 40},
				\ {'name' : 's1',
				\  'buffer' : 'a.c',
				\  'lnum' : 50}
				\ ])
<
		Peut également être utilisée comme méthode |method| : >
			GetSignlist()->sign_placelist()

sign_undefine([{name}])					*sign_undefine()*
sign_undefine({list})
		Supprime une signalisation {name} précédemment définie.
		Similaire à la commande |:sign-undefine|. Si {name} n'est
		pas fourni, supprime toutes les signalisations définies.

		L'unique argument {list} peut être utilisé pour supprimer
		la définition d'une liste de signalisations. Chaque élément
		de la liste est le nom d'une signalisation.

		Renvoie 0 en cas de succès, et -1 en cas d'échec. Pour l'appel
		de l'argument {list}, renvoie une liste de valeurs, une pour
		chaque signalisation ayant sa définition supprimée.

		Exemples: >
			" Supprime une signalisation nommée mySign
			call sign_undefine("mySign")

			" Supprime les signalisations 'sign1' et 'sign2'
			call sign_undefine(["sign1", "sign2"])

			" Supprime toutes les signalisations
			call sign_undefine()
<
		Peut également être utilisée comme méthode |method| : >
			GetSignlist()->sign_undefine()

sign_unplace({group} [, {dict}])			*sign_unplace()*
		Supprime une signalisation précédemment placée dans un ou 
		plusieurs tampons. Similaire à la commande |:sign-unplace|.

		{group} est le nom du groupe de signalisation. Pour utiliser
		le groupe de signalisation global, utilisez la chaîne vide.
		Si {group} vaut '*', tous les groupes y compris le groupe
		global sont utilisés.
		Les signalisation dans {group} sont sélectionnées sur la 
		base des entrées dans {dict}. Les entrées optionelles de
		{dict} supportées sont les suivantes :
			buffer	nom ou numéro de tampon. Voir |bufname()|.
			id	identifiant de signalisation
		Si {dict} est absent, toutes les signalisations de {group}
		sont supprimées.

		Renvoie 0 sur un succès, -1 en cas d'échec.

		Exemples: >
			" Supprime la sign. 10 du tampon a.vim
			call sign_unplace('', {'buffer' : "a.vim", 'id' : 10})

			" Supprimer la sign. 20 dans le groupe 'g1' du tampon 3
			call sign_unplace('g1', {'buffer' : 3, 'id' : 20})

			" Supprime toutes les signalisations dans le groupe
			" 'g2' du tampon 10
			call sign_unplace('g2', {'buffer' : 10})

			" Supprime la signalisation 30 dans le groupe 'g3' de
			" tous les tampons
			call sign_unplace('g3', {'id' : 30})

			" Supprime toutes les sign. placees dans le tampon 5
			call sign_unplace('*', {'buffer' : 5})

			" Supprime les signalisation du groupe 'g4' de tous
			" les tampons
			call sign_unplace('g4')

			" Supprime la signalisation 40 de tous les tampons
			call sign_unplace('*', {'id' : 40})

			" Supprime toutes les signalisations signées de tous
			" les tampons
			call sign_unplace('*')
<
		Peut également être utilisée comme méthode |method| : >
			GetSigngroup()->sign_unplace()
<
sign_unplacelist({list})				*sign_unplacelist()*
		Supprime les signalisations placées d'un ou plusieurs tampons.
		Similaire la la fonction |sign_unplace()|.

		L'argument {list} désigne la liste de signalisation à
		supprimer. Chaque élément de la liste est un dictionnaire
		avec les attributs de signalisations suivants :
		    buffer	nom ou numéro de tampon. Pour les valeurs 
				acceptées voir |bufname()|. Si absent, la 
				signalisation est supprimées de tous les 
				tampons.
		    group	nom de groupe de signalisation. Si absent ou
				égal à la chaîne vied, le groupe global est
				utilisé. Si égal à '*', tous les groupes y
				compris le global sont utilisés.
		    id		identifiant de signalisation. Si absent, toutes
				les signalisations du groupe sont supprimées

		Renvoie une liste de codes de retour valant 0 si la signalisation
		correspondante a été supprimée avec succès, ou -1 en cas
		d'échec.

		Exemple: >
			" Supprime la signalisation d'id 10 du tampon a.vim
			" et celle d'id 20 du tampon b.vim
			call sign_unplacelist([
				\ {'id' : 10, 'buffer' : "a.vim"},
				\ {'id' : 20, 'buffer' : 'b.vim'},
				\ ])
<
		Peut également être utilisée comme méthode |method| : >
			GetSignlist()->sign_unplacelist()
<

 vim:tw=78:ts=8:noet:ft=help:norl:
