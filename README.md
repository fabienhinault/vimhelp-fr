Ce projet a pour but de mettre à jour la traduction de l'aide en ligne de Vim.

Le point de départ est la version 6.2, la version d'arrivée 8.2.

Les différences entre les versions en anglais ont été insérée dans les traductions,
approximativement à l'endroit où la modification doit être faite dans la 
version française.
regex utile 
`    /^[0-9]\+,\?[0-9]*\(c\|a\|d\)`
```
map <F6> /^[0-9]\+,\{0,1\}[0-9]*\(a\\|c\\|d\)<CR>zt/\(a\\|c\\|d\)<CR>l"ayt,<C-W>w:<C-R>a<CR>zt<C-W>w
map <F7> ?^[0-9]\+,\{0,1\}[0-9]*\(a\\|c\\|d\)<CR>
map <F8> :.,/^\([^<>-]\\|$\)/-1 d<CR>
map <F9> xxj0

:% s/^\([0-9]\+,\{0,1\}[0-9]*\(a\|c\|d\)[0-9]\+\)$/\1,




   <F2>          :set nowrapscan<CR><F8>p<F6><F2>
   <F3>          :set nowrapscan<CR><F8>kP<F6><F3>
   <F4>          gg<F6>
   <F6>          /^[0-9]\+,\{0,1\}[0-9]*\(a\|c\|d\)<CR>  
   <F7>          ?^[0-9]\+,\{0,1\}[0-9]*\(a\|c\|d\)<CR>
   <F8>          :.,/^\([^<>-]\|$\)/-1 d<CR>
   <F9>          0xxj0
   <F10>         <F8>p
   <F11>         <F8>kP
   <F12>         0xx
```

Pour les fichiers qui n'existaient pas du tout dans l'ancienne traduction, le fichier
.txt en anglais est présent.

Fichiers traduits
- usr_01
- usr_02
- usr_03.frx:23
- usr_04
- usr_05
- usr_06
- usr_07
- usr_08
- usr_09
- usr_10.frx
- usr_11.frx
- usr_12.frx:8
- ...
- usr_30
- usr_31
- usr_32
- usr_40
-
- usr_42
- usr_43
- usr_44
- usr_45
- usr_90.frx:29
- usr_toc.frx
- debugger.frx:23
- develop.frx:23
- diff.frx:43
- digraph.frx:19
- farsi.frx:5
- filetype.frx:49
- fold.frx:31
- gui_w16.frx
- gui_w32.frx:27
- gui_x11.frx:25
- hebrew.frx:5
- help.frx
- howto.frx
- if_cscop.frx:25
- if_ole.frx:14
- if_perl.frx:21
- if_ruby.frx:35
- if_sniff.frx
- if_tcl.frx:26
- hangulin.frx:4
- mlang.frx:17
- os_390.frx:24
- os_msdos.frx:4
- os_os2.frx:
- os_risc.frx:4
- version4.frx
- workshop.frx
- os_amiga.frx:10
- os_beos.frx:5
- os_dos.frx:10
- os_mac.frx:13
- os_mint.frx:6
- os_qnx.frx:5
- os_unix.frx:7
- os_win32.frx:20
- pi_expl.frx
- pi_gzip.frx:4
- pi_spec.frx:13
- quickref.frx:44
- rileft.frx:6
- recover.frx:12
- remote.frx:13
- scroll.frx:30
- sign.frx:33
- tips.frx:22
- uganda.frx:23
- undo.frx:13
- visual.frx:29

En cours

- term.frx:49
- repeat.frx:50

Fichiers restant à traduire, dans un ordre encourageant

- message.frx:53
- mbyte.frx:64
- quotes.frx:64
- indent.frx:65
- version5.frx:66
- gui.frx:68
- intro.frx:68
- autocmd.frx:71
- pi_netrw.frx:72
- netbeans.frx:73
- os_vms.frx:83
- vi_diff.frx:87
- insert.frx:92
- tagsrch.frx:99
- cmdline.frx:104
- various.frx:104
- motion.frx:109
- windows.frx:109
- quickfix.frx:116
- pattern.frx:119
- version6.frx:127
- usr_41.frx:128
- map.frx:146
- starting.frx:146
- editing.frx:159
- index.frx:184
- change.frx:194
- todo.frx:362
- syntax.frx:393
- eval.frx:583
- arabic.frx:702
- options.frx:933
- if_pyth.frx:49
