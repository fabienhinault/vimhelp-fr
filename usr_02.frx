*usr_02.txt*	Pour Vim version 8.2. Dernier changement 21 nov 2019

	       MANUEL de l'UTILISATEUR VIM - par Bram Moolenaar

			    Premiers pas dans Vim


Ce chapitre donne les informations nécessaires pour éditer un fichier avec
Vim. Ni très bien ni très vite, mais vous pourrez éditer. Prenez un peu de
temps pour vous exercer avec ces commandes, elles constituent la base de tout
ce qui suit.

|02.1|	Lancer Vim pour la première fois
|02.2|	Insérer du texte
|02.3|	Se déplacer
|02.4|	Effacer des caractères
|02.5|	Annuler et restaurer
|02.6|	Autres commandes d'édition
|02.7|	Quitter
|02.8|	Trouver de l'aide

  Chapitre suivant : |usr_03.txt|  Se déplacer dans le texte
Chapitre précédent : |usr_01.txt|  Introduction aux manuels
Table des matières : |usr_toc.txt|

==============================================================================
*02.1*	Lancer Vim pour la première fois

Pour lancer Vim, entrez cette commande : >

	gvim fichier.txt

Sous Unix, vous pouvez taper ceci après n'importe quelle invite de commande.
Si vous utilisez MS-Windows, ouvrez une fenêtre d'invite de commande et
entrez la commande.
   Dans tous les cas, Vim lance l'édition d'un fichier nommé "fichier.txt".
Comme il s'agit d'un nouveau fichier, vous verrez une fenêtre vide. Votre
écran devrait ressembler à ceci :

	+---------------------------------------+
	|#					|
	|~					|
	|~					|
	|~					|
	|~					|
	|"fichier.txt" [New file]		|
	+---------------------------------------+
	   ("#" représente la position du curseur.)

Les tildes ('~') indiquent des lignes qui ne font pas partie du fichier. En
d'autres termes, quand Vim n'a plus de lignes à afficher, il affiche des
tildes. Au bas de l'écran, une ligne de message indique que le fichier
s'appelle "fichier.txt" et que c'est un nouveau fichier. Ce message
d'information est temporaire et de nouvelles informations viendront le
remplacer.


LA COMMANDE VIM

La commande gvim provoque la création d'une nouvelle fenêtre pour l'édition.
Si vous utilisez cette commande >

	vim fichier.txt

l'édition se déroulera à l'intérieur de votre fenêtre de commande.
C'est-à-dire que si vous travaillez dans un xterm, Vim utilisera votre fenêtre
xterm ; si vous utilisez une fenêtre d'invite de commande MS-Windows, l'édition
se déroulera dedans. Le texte dans la fenêtre sera identique pour les deux
versions, mais avec gvim, vous pouvez bénéficier de fonctionnalités
supplémentaires, telle que la barre de menus. Nous y reviendrons ultérieurement.

==============================================================================
*02.2*	Insérer du texte

Vim est un éditeur modal. Cela signifie qu'il se comportera différemment selon
le mode dans lequel vous êtes. Les deux modes de base sont le mode Normal et
le mode Insertion. En mode Normal, les caractères que vous tapez sont des
commandes. En mode Insertion, les caractères sont insérés en tant que texte.
   Comme vous venez juste de lancer Vim, il sera en mode Normal (par défaut).
Pour lancer le mode Insertion, tapez la commande "i" ('i' comme Insérer). Vous
pouvez ensuite entrer du texte : il sera inséré dans le fichier. Ne vous
inquiétez pas si vous faites des erreurs : vous pourrez les corriger plus
tard. Pour entrer l'adage suivant, voici ce qu'il faut taper : >

	iUn petit chez soi vaut mieux
	Qu'un grand chez les autres

Après avoir tapé "mieux", pressez la touche <Entree> pour commencer une
nouvelle ligne. Finalement, pressez la touche <Echap> pour quitter le mode
Insertion et revenir en mode Normal. Vous avez maintenant deux lignes de texte
dans votre fenêtre Vim :

	+---------------------------------------+
	|Un petit chez soi vaut mieux		|
	|Qu'un grand chez les autres		|
	|~					|
	|~					|
	|					|
	+---------------------------------------+


QUEL EST LE MODE COURANT ?

Pour voir dans quel mode vous êtes, tapez cette commande : >

	:set showmode

Vous remarquerez que quand vous tapez le deux-points, Vim place le curseur sur
la dernière ligne de la fenêtre. C'est l'endroit où vous pouvez entrer les
commandes deux-points (commandes qui débutent par ':'). Terminez cette
commande en pressant la touche <Entree> (toutes les commandes deux-points se
terminent de la sorte).
   À présent, si vous tapez la commande "i", Vim affichera "--INSERT--" au bas
de la fenêtre. Ceci indique que vous êtes en mode Insertion.

	+---------------------------------------+
	|Un petit chez soi vaut mieux		|
	|Qu'un grand chez les autres		|
	|~					|
	|~					|
	|-- INSERT --				|
	+---------------------------------------+

Si vous pressez <Echap> pour revenir en mode Normal, la dernière ligne sera
effacée.


EN CAS DE FAUSSE MANOEUVRE

Un des problèmes souvent rencontré par les novices est la confusion des modes,
causée par l'oubli du mode courant ou par la saisie accidentelle d'une
commande qui fait basculer dans un mode. Pour revenir en mode Normal, quel que
soit le mode dans lequel vous êtes, pressez la touche <Echap>. Parfois, vous
devrez la presser deux fois. Si Vim émet un bip, c'est que vous êtes déjà en
mode Normal.

==============================================================================
*02.3*	Se déplacer

Après être revenu en mode Normal, vous pouvez vous déplacer dans le texte en
utilisant ces touches :

	h   gauche					    *hjkl*
	j   bas
	k   haut
	l   droite

Au premier abord, il peut sembler que ces commandes aient été choisies un peu
au hasard. Après tout, qui a déjà utilisé "l" mis pour droite ? Il existe en
fait une bonne raison pour expliquer ces choix : le déplacement du curseur est
l'action la plus courante dans un éditeur de texte, et ces touches se trouvent
alignées juste sous votre main droite. En d'autres termes, ces commandes sont
placées là où vous pouvez accéder le plus rapidement (en particulier si vous
tapez avec vos dix doigts).

	NOTE :
	Vous pouvez aussi déplacer le curseur en utilisant les touches
	fléchées. Mais si vous faites cela, vous ralentissez grandement votre
	édition car pour atteindre les touches fléchées, vous devez déplacer
	votre main du pavé texte au pavé curseur. En supposant que vous le
	fassiez plusieurs centaines de fois en une heure, vous perdriez un
	temps considérable.
	   De plus, il existe des claviers qui sont dépourvus de touches
	fléchées, ou qui les situent à des emplacements inhabituels ; savoir
	utiliser les touches hjkl vous rendra un grand service dans ces
	situations.

Il est assez facile de retenir ces commandes si l'on songe que sur le clavier,
"h" est sur la gauche, "l" sur la droite et "j" pointe vers le bas. Avec
un dessin : >

		       k
		   h     l
		     j

La meilleure façon d'apprendre ces commandes est de pratiquer. Utilisez la
commande "i" pour insérer des lignes supplémentaires de texte. Puis utilisez
les touches hjkl pour vous déplacer et insérer un mot quelque part. N'oubliez
pas de presser <Echap> pour revenir en mode Normal. Le tutoriel |vimtutor| est
également un bon moyen pour vous entraîner.

Pour les utilisateurs japonais, Hiroshi Iwatani propose ce moyen
mnémotechnique :

			    Komsomolsk
				^
			        |
	       Huan Ho      <--- --->  Los Angeles
	   (Fleuve Jaune)	|
				v
			      Java (l'île, pas le langage de programmation)

==============================================================================
*02.4*	Effacer des caractères

Pour effacer un caractère [N.D.T. : en fait, le couper], amenez le curseur
dessus et tapez "x". (C'est un héritage des anciennes machines à écrire, quand
on biffait du texte en tapant xxxx en surfrappe.) Par exemple, amenez le
curseur au début de la première ligne et tapez "xxxxxxxxx" (neuf 'x') pour
effacer "Un petit ". Le résultat devrait ressembler à ceci :

	+---------------------------------------+
	|chez soi vaut mieux			|
	|Qu'un grand chez les autres		|
	|~					|
	|~					|
	|					|
	+---------------------------------------+

À présent vous pouvez insérer un nouveau texte, en tapant par exemple : >

	iUn moyen <Echap>

Ceci lance une insertion (le "i"), insère les mots "Un moyen ", puis quitte le
mode Insertion (le <Echap> final). On obtient :

	+---------------------------------------+
	|Un moyen chez soi vaut mieux		|
	|Qu'un grand chez les autres		|
	|~					|
	|~					|
	|					|
	+---------------------------------------+


SUPPRIMER UNE LIGNE

Pour effacer une ligne entière [N.D.T. : la couper], utilisez la commande "dd"
["Delete"]. La ligne suivante sera alors déplacée vers le haut pour combler le
vide :

	+---------------------------------------+
	|Qu'un grand chez les autres		|
	|~					|
	|~					|
	|~					|
	|					|
	+---------------------------------------+


SUPPRIMER UNE COUPURE DE LIGNE

Vim peut fusionner deux lignes ensemble, ce qui signifie que la coupure de
ligne les séparant sera supprimée. Utilisez la commande "J" ["Join"] pour
cela. Prenez ces deux lignes :

	Un petit chez soi vaut ~
	mieux ~

Amenez le curseur sur la première ligne et pressez "J" :

	Un petit chez soi vaut mieux ~

==============================================================================
*02.5*	Annuler et restaurer

Supposons que vous ayez effacé trop de texte. Vous pourriez bien sûr le
saisir à nouveau, mais un moyen plus facile existe. La commande "u" ["Undo"]
annule la dernière édition. Par exemple : après avoir utilisé "dd" pour
effacer la première ligne, "u" la ramènera.
   Un autre exemple : placez le curseur sur le 'U' de la première ligne :

	Un petit chez soi vaut mieux ~

Tapez maintenant "xxxxxxxx" pour effacer "Un petit". Vous obtenez :

	 chez soi vaut mieux ~

Tapez "u" pour annuler le dernier effacement. Celui-ci avait effacé le 't',
donc l'annulation restaurera ce caractère :

	t chez soi vaut mieux ~

La commande "u" suivante restaure l'avant-dernier caractère effacé :

	it chez soi vaut mieux ~

La commande "u" suivante donne le t, et ainsi de suite :

	tit chez soi vaut mieux ~
	etit chez soi vaut mieux ~
	petit chez soi vaut mieux ~
	 petit chez soi vaut mieux ~
	n petit chez soi vaut mieux ~
	Un petit chez soi vaut mieux ~

	Note :
	Si vous tapez "u" deux fois et que vous retombez sur le même texte,
	c'est que Vim est configuré pour fonctionner de façon compatible Vi.
	Reportez-vous à |not-compatible| pour régler ce problème.
	   Cette section suppose que vous utilisez « la méthode Vim ». Si vous
	préférez la bonne vieille méthode Vi, vous devrez faire attention à de
	petites différences de comportement.


RESTAURER

Si vous annulez un trop grand nombre de fois, vous pouvez presser CTRL-R
["Redo"] pour inverser la commande précédente. En d'autres termes, cela annule
l'annulation. Pour le voir en pratique, pressez CTRL-R deux fois. Les
caractères 'U' et 'n' disparaîtront :

	 petit chez soi vaut mieux ~

Il existe une version spéciale de commande d'annulation, la commande "U"
["Undo line"]. Elle annule tous les changements effectués sur la dernière
ligne éditée. Taper cette commande deux fois annule le précédent "U".

	Un petit chez soi vaut mieux ~
			      xxxxxx	    Supprime " mieux"
	Un petit chez soi vaut ~
		 xxxxxxxxx		    Supprime "chez soi "
	Un petit vaut ~
					    Restaure la ligne avec "U"
	Un petit chez soi vaut mieux ~
					    Annule "U" avec "u"
	Un petit vaut ~

La commande "U" est un changement en elle-même, elle peut donc être annulée
avec "u" et restaurée avec CTRL-R. Cela peut paraître compliqué, mais soyez
sans crainte : avec "u" et CTRL-R, vous pouvez revenir à n'importe quelle
situation précédente. Plus de détails en section |32.2|.

==============================================================================
*02.6*	Autres commandes d'édition

Vim dispose de nombreuses commandes pour changer du texte. Voir |Q_in| et
ci-dessous. Vous trouverez ici les plus utilisées.


AJOUTER DU TEXTE

La commande "i" insère un caractère avant la position du curseur. Cela marche
bien ; mais comment faire quand vous voulez ajouter du texte à la fin de la
ligne ? Pour cela, vous devez insérer du texte après le curseur. C'est
possible avec la commande "a" ["Append"].
   Par exemple, pour changer la ligne

	L'idéal restant un grand chez soi ! ~
en
	L'idéal restant un grand chez soi... ~

amenez le curseur sur le point d'exclamation à la fin de la ligne. Puis tapez
"xx" pour effacer ce point d'exclamation et l'espace précédent. Le curseur est
à présent placé à la fin de la ligne, sur le 'i' de "soi". Tapez >

	a...<Echap>

pour ajouter trois points de suspension après le 'i' de "soi" :

	L'idéal restant un grand chez soi... ~


OUVRIR UNE NOUVELLE LIGNE

La commande "o" crée une nouvelle ligne vide sous le curseur et lance le mode
Insertion. Vous pouvez ensuite entrer du texte dans cette nouvelle ligne.
   Supposons que le curseur soit quelque part dans la première de ces deux
lignes :

	Un petit chez soi vaut mieux ~
	Qu'un grand chez les autres ~

Maintenant, si vous utilisez la commande "o" et tapez du texte >

	o(Surtout avec Vim)<Echap>

vous obtenez le résultat suivant :

	Un petit chez soi vaut mieux ~
	(Surtout avec Vim) ~
	Qu'un grand chez les autres ~

La commande "O" (en majuscule) ouvre une ligne au-dessus du curseur.


UTILISER UN QUANTIFICATEUR

Supposons que vous vouliez remonter de neuf lignes. Vous pouvez taper
"kkkkkkkkk", ou bien entrer la commande "9k". En fait, vous pouvez faire
précéder de nombreuses commandes par un nombre, appelé quantificateur. Un
peu plus haut dans ce chapitre, par exemple, nous avons ajouté trois points de
suspension à la fin d'une ligne avec "a...<Echap>". Mais on aurait également
pu faire "3a.<Echap>". Le nombre 3 indique à la commande qui suit de tripler
son effet. De la même façon, pour effacer trois caractères, utilisez la
commande "3x". Le quantificateur vient toujours avant la commande à laquelle
il s'applique.

==============================================================================
*02.7*	Quitter

Pour quitter, utilisez la commande "ZZ". Cette commande enregistre le fichier
et quitte.

	NOTE :
	Contrairement à de nombreux autres éditeurs, Vim ne crée pas
	automatiquement de fichier de sauvegarde. Si vous tapez "ZZ", vos
	changements seront enregistrés et vous ne pourrez plus revenir en
	arrière. Vous pouvez configurer Vim pour qu'il crée des fichiers de
	sauvegarde ; voir |07.4|.


IGNORER LES CHANGEMENTS

Parfois, on effectue toute une série de changements et puis on réalise que,
vraiment, les choses étaient mieux avant. Pas de problème, Vim a une commande
pour quitter-et-jeter-les-changements. Il s'agit de : >

	:q!

N'oubliez pas de presser <Entree> pour terminer la commande.

Pour ceux que les détails intéressent, les trois parties de cette commande
sont le deux-points (':'), qui fait basculer sur la ligne de commande ; la
commande 'q', qui demande à l'éditeur de quitter ; et le modificateur '!', qui
force la commande.
   Forcer la commande est nécessaire, car Vim est réticent à la perte de
changements. Si vous aviez simplement tapé ":q", Vim aurait affiché un message
d'erreur et refusé de quitter :

	E37: No write since last change (use ! to override) ~

En utilisant '!', vous dites en quelque sorte à Vim : « Je sais que ce que je
fais a l'air idiot, mais je veux vraiment le faire. »

Si vous voulez continuer l'édition avec Vim : la commande ":e!" rechargera la
version originale du fichier.

==============================================================================
*02.8*	Trouver de l'aide

Tout ce que vous avez toujours voulu savoir sur Vim peut être trouvé dans les
fichiers d'aide de Vim. N'ayez pas peur de demander !

Si vous savez ce que vous chercher, il est généralement plus facile de
le chercher dans le système d'aide que d'utiliser Google, parce que les sujets
suivent un certain style.

L'aide a également l'avantage d'appartenir à votre version particulière de Vim.
Vous ne verrez pas d'aide pour des commandes ajoutées plus tard, et qui ne 
fonctionneraient pas pour vous.

Pour obtenir une aide générique, utilisez cette commande : >

	:help

Vous pouvez aussi utiliser la première touche de fonction, <F1>. Si votre
clavier a une touche <Aide>, elle devrait également fonctionner.
   Si vous ne donnez pas de sujet, ":help" affiche la fenêtre d'aide
générique. Les créateurs de Vim ont conçu le système d'aide de façon
particulièrement ingénieuse (ou particulièrement paresseuse) : la fenêtre
d'aide est une simple fenêtre d'édition. Vous pouvez donc utiliser toutes les
commandes normales de Vim pour vous déplacer dans l'aide. Ainsi "h", "j", "k",
et "l" déplacent à gauche, bas, haut et droite.
   Pour quitter la fenêtre d'aide, utilisez la même commande que pour quitter
l'éditeur : "ZZ". Cela fermera juste la fenêtre d'aide, mais ne quittera pas
Vim.

En lisant le texte de l'aide, vous remarquerez que certains mots sont encadrés
par des barres verticales (par exemple, |help|). Ceci représente un hyperlien.
Si vous placez le curseur n'importe où entre les barres et pressez CTRL-]
(sauter vers un marqueur), le système d'aide vous amènera au sujet en
question. (Pour des raisons que nous n'aborderons pas ici, la terminologie
qu'utilise Vim pour hyperlien est marqueur. Ainsi, CTRL-] saute à
l'emplacement du marqueur donné par le mot sous le curseur.)
   Après plusieurs sauts, vous voudrez peut-être revenir en arrière. CTRL-T
(dépile le marqueur) vous ramène à la position précédente. CTRL-O (saute à une
position plus ancienne) fonctionne également très bien ici.
   Au sommet de l'écran d'aide, vous verrez la notation *help.txt*. Ce nom
entre caractères '*' est utilisé par le système d'aide pour définir un
marqueur (cible de l'hyperlien).
   Voir |29.1| pour plus de détails sur l'utilisation des marqueurs.

Pour obtenir de l'aide sur un sujet particulier, utilisez la commande
suivante : >

	:help {sujet}

Par exemple, pour obtenir de l'aide sur la commande "x", entrez : >

	:help x

Si vous cherchez comment couper du texte, utilisez cette commande : >

	:help deleting

Pour avoir un index complet de toutes les commandes Vim, tapez : >

	:help index

Quand vous recherchez de l'aide sur une commande avec un caractère de contrôle
(par exemple, CTRL-A), vous devez l'épeler avec le préfixe "CTRL-". >

	:help CTRL-A

L'éditeur Vim a de nombreux modes différents. Par défaut, le système d'aide
donne les commandes du mode Normal. Par exemple, la commande suivante donne
l'aide de la commande CTRL-H du mode Normal : >

	:help CTRL-H

Pour préciser un autre mode, utilisez un préfixe. Par exemple, utilisez "i_"
pour une commande du mode Insertion. Pour connaître l'action de CTRL-H dans ce
mode, entrez : >

	:help i_CTRL-H

Quand vous lancez l'éditeur Vim, vous pouvez utiliser plusieurs arguments de
commande. Ils débutent tous par un tiret ('-'). Pour savoir ce que fait
l'argument -t, par exemple, utilisez la commande : >

	:help -t

Vim dispose de nombreuses options qui vous permettent de le configurer et de
le personnaliser. Si vous recherchez de l'aide sur une option, vous devez
encadrer son nom entre apostrophes simples. Par exemple, pour l'option
'number' : >

	:help 'number'

Pour voir un tableau regroupant tous les préfixes des modes, consultez
|help-summary| ci-dessous.

Les touches spéciales sont encadrées par des chevrons : '<' et '>'. Ainsi,
pour trouver de l'aide sur la touche flèche-haut en mode Insertion, utilisez
cette commande : >

	:help i_<Haut>

Si vous voyez un message d'erreur que vous ne comprenez pas, par exemple

	E37: No write since last change (use ! to override) ~

vous pouvez utiliser son numéro d'ID (au début du message) pour obtenir une
aide dessus : >

	:help E37


Résumé : 					*help-summary*  >

1) Utilisez Ctrl-D après avoir saisi un sujet et laissez Vim montrer to les sujets
   dispondibles.
   Ou pressez Tab pour compléter : >
	:help some<Tab>
<   Plus d'information sur l'utilisation de l'aide : >
	:help helphelp

2) Suivez les liens entre barres pour les sujets connexes. Vous pouvez aller de
   l'aide détaillée à la documentation utilisateur, qui décrit certaines 
   commandes plus du point de vue de l'utilisateur, et de manière moins détaillée.
   Par exemple : >
	:help pattern.txt
<   Vous pouvez voir les partie |03.9| et |usr_27.txt| du guide utilisateur dans
   l'introduction.

3) Les options sont entre apostrophes. Pour se rendre à la partie concernant l'option
list : >
   list option: >
	:help 'list'
<   Si tout ce que vous savez est que vous cherchez une option, vous pouvez aussi
faire : >
	:help options.txt
<   pour ouvrir la page d'aide qui décrit toute la gestion d'option, puis chercher
   en utilisant les expressions régulières, par exemple "largeur".
   Certaines options ont leur propre espace de nommage, par exemple : >
	:help cpo-<lettre>
<   pour le marqueur correspondant dans les paramètres 'cpoptions', remplacez 'lettre'
   par un marqueur spécifiqe, par exemple : >
	:help cpo-;
<   Pour les marqueurs des options de l'interface graphique (guioption) : >
	:help go-<letter>

4) Les commandes du mode normal n'ont pas de préfixe. Pour se rendre à la page d'aide
de la commande "gt" : >
	:help gt

5) Les commandes du mode insertion commencent par i_. L'aide pour supprimer un mot : >
	:help i_CTRL-W

6) Les commandes du mode visuel commencent par v_. L'aide pour sauter vers l'autre
  côté de l'aire visuelle : >
	:help v_o

7) L'édition en ligne de commande et les arguments commencent par c_. L'aide sur
  l'utilisation de l'argument de commande % : >
	:help c_%

8) Les commandes ex commencent toujours par ":", ainsi, pour se rendre à l'aide de
  la commande ":s" : >
	:help :s

9) Les commandes spécifiques au débuggage commencent par ">". Pour atteindre l'aide
   de la commande de débug "cont" : >
	:help >cont
 
10) Combinaisons de touches. Elles commencent habituellement par une seule lettre 
   indiquant le mode pour lequel elles peuvent être utilisées. Par exemple : >
	:help i_CTRL-X
<    vous emmène chez la famille des commandes CTRL-X pour le mode insertion
    qui peuvent être utilisées pour l'auto-complétion de différentes choses.
    Notez que certaines touches seront toujours écrites de la même façon. Par
    exemple, Contrôle sera toujours CTRL.
    Pour les commande du mode normal, il n'y a pas de préfixe, et le sujet est
    à CTRL-<Lettre>. Par exemple >
    :h CTRL-<Letter>. E.g.  >
	:help CTRL-W
<    Par contraste >
	:help c_CTRL-R
<    décrit ce que fait CTRL-R lorsqu'on tape une commande dans la ligne de 
    commande et >
	:help v_CTRL-A
<    traite de l'incrémentation en mode visuel et >
	:help g_CTRL-A
<    de la commande "g<C-A>" (appuiez sur "g", puis <CTRL-A>). Ici, la lettre
    "g" représente la commande normale "g" qui attends toujours une deuxième
    touche avant de faire une chose similaire aux commandes commençant par "z".

11) Les entrées concernant les expressions régulières commencent toujours par /.
   Pour recevoir de l'aide à propos du quantificateur "\+" dans les expressions
   régulières de Vim : >
	:help /\+
<    Si vous voulez tout savoir sur les expressions régulières, commencez par 
    lire : >
	:help pattern.txt

12) Les registres commencent toujours par "quote". Pour découvrir le registre
    spécial ":" : >
	:help quote:

13) Vim script se trouve à >
	:help eval.txt
<    Certains aspects du langage se trouvent à :h expr-X , "X" étant
    une lettre seule. Par exemple : >
	:help expr-!
<    vous emmène à la description de l'opérateur "!" (Not) pour Vim
    script.
    Egalement important est >
	:help function-list
<    pour découvrir une brève description de toutes les fonctions existantes.
    Les articles de l'aide concernant les fonctions de Vim script functions
    se terminent toutes par "()", ainsi : >
	:help append()
<    traite de la fonction Vim script "append", et non de l'adjonction de
    texte dans le buffer courant.

14) Les mappage sont le sujet de la page d'aide :h |map.txt|. Tapez >
	:help mapmode-i
<    pour découvrir la commande |:imap|.  Essayez aussi :map-topic
    pour découvrir certaines pages concernant les mappages. Par exemple : >
	:help :map-local
<    pour les mappages locaux au buffer, ou >
	:help map-bar
<    sur le traitement de '|' dans les mappages.

15) Les définitions de commandes sont traitées sur :h command-sujet . Tapez >
	:help command-bar
<    pour découvrir l'argument '!' des commandes sur-mesure.

16) Les commandes de gestion des fenêtres commencent toujours par CTRL-W,
découvrez l'aide correspondante avec :h CTRL-W_lettre.  Par exemple : >
	:help CTRL-W_p
<    pour le retour à la fenêtre précédente. Vous pouvez également accéder à >
	:help windows.txt
<    et continuer votre lecture si vous cherchez des commandes sur la
    manipulation des fenêtres.

17) Utilisez |:helpgrep| pour effectuer une recherche sur toutes les pages
    d'aide (et aussi sur tous les greffons installés). Voir |:helpgrep| sur
    son utilisation.
    Pour effectuer une recherche sur un sujet : >
	:helpgrep sujet
<    vous emmène à la première entrée correspondante. Pour la suivante : >
	:cnext
<    Tous les résultats peuvent être obtenus dans la fenêtre quickfix que
    l'on ouvre par : >
	:copen
<    Déplacez vous près du résultat voulu et tapez Entrée pour aller vers
    cette aide.

18) Le manuel utilisateur. Ceci décrit l'aide destinée aux débutant dans un
    style amical. Commencez par la table des matière |usr_toc.txt| : >
	:help usr_toc.txt
<    Parcourrez son contenus pour découvrir les sujets qui vous intéressent.
    Les sujets "Digrammes" et "Saisir des caractères spéciaux" sont traités 
	au chapitre 24. Pour vous rendre à cette page : >
	:help usr_24.txt
<    Si vous souhaitez vous rendre à une section particulière de l'aide, le
    numéro de la section peut être entré directement. Par >
	:help 10.1
<    vous êtes directement envoyé à la section 10.1 de |usr_10.txt|, à propos
    de l'enregistrement des macros.

19) Groupes de coloration. Commencez toujour par hl-groupname.  Par 
    exemple >
	:help hl-WarningMsg
<    traite du groupe de coloration WarningMsg.

20) La coloration sytaxique suit le schéma  :syn-sujet. Par exemple >
	:help :syn-conceal
<    traite de l'argument conceal pour la commande syn.

21) Les commande quickfix commenent généralement par :c, et les commandes
de liste d'emplacement par :l

22) Les évènements autocommand events répondent à leurs noms : >
	:help BufWinLeave
<    Pour voir tous les évènements : >
	:help autocommand-events

23) Les arguments de ligne de commande commencent toujours par "-". Ainsi
    pour l'aide concernant l'argument -f, utilisez : >
	:help -f

24) Les fonctionnalités optionnelles commencent toujours par "+". Pour
    découvrir la foncionnalité de dissimulation, utilisez : >
	:help +conceal

25) La documentation relative à une fonctionnalité spécifique à un type
    de fichier est généralement accessible par un appel de la forme
	ft-<type_fichier>-<fonctionnalité>. Ainsi
	:help ft-c-syntax
<    traite du fichier de syntaxe pour le langage C et de l'option qu'il
    fournit. Parfois, des sections supplémentaires pour la complétion
	omniprésente >
	:help ft-php-omni
<    ou des greffons de type de fichier >
	:help ft-tex-plugin
<    sont également disponibles.

26) Les codes d'erreurs et d'avertissement peuvent être consultés directement
    dans l'aide. Ainsi >
	:help E297
<    vous emmène précisément à la description du message d'erreur de swap et >
	:help W10
<    traite de l'avertissement "Modification d'un fichier en lecture seule".
    Parfois, cependant, ces erreurs ne sont pas décrites, mais seulement listées
    avec la commande qui les causes habituellement. Ainsi : >
	:help E128
<    vous envoie à la commande |:function|.


==============================================================================

Chapitre suivant : |usr_03.txt|  Se déplacer dans le texte

Copyright: voir |manual-copyright|  vim:tw=78:ts=8:noet:ft=help:norl:
